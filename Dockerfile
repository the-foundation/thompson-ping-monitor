FROM webdevops/php-nginx
RUN apt-get update && apt install -y iputils-ping miredo dnsmasq
RUN /bin/sh -c "(echo no-resolv;echo bogus-priv;echo strict-order;echo server=9.9.9.9;echo server=2620:fe::fe;echo server=2606:4700:4700::1111;echo server=1.1.1.1) | tee -a /etc/dnsmasq.conf ; echo nameserver 127.0.0.1 > /etc/resolv.conf||true"

CMD /bin/bash -c "sysctl net.ipv6.conf.all.disable_ipv6=0 & /etc/init.d/dnsmasq start & echo nameserver 127.0.0.1 > /etc/resolv.conf & sysctl net.ipv6.conf.all.disable_ipv6=0 || true && /etc/init.d/miredo start &  /usr/bin/python /usr/bin/supervisord -c /opt/docker/etc/supervisor.conf --logfile /dev/null --pidfile /dev/null --user root & sleep 15 ; ip -6 a|grep -q global || /etc/init.d/miredo restart  ; wait;fg"
